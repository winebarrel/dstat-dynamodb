# dstat-dynamodb.git

[Dstat](http://dag.wiee.rs/home-made/dstat/) plugin for [Amazon DynamoDB](https://aws.amazon.com/dynamodb/).

## Installation

```sh
$ sudo pip install boto
$ sudo pip install pytz
$ wget https://bitbucket.org/winebarrel/dstat-dynamodb/raw/master/dstat_dynamodb.py
$ sudo mv dstat_dynamodb.py /usr/share/dstat/
```

## Usage

```sh
$ export AWS_ACCESS_KEY_ID=...
$ export AWS_SECRET_ACCESS_KEY=...
$ export AWS_REGION=...
$ DSTAT_DYNAM0DB_TABLE=employees dstat -t --dynamodb 20
----system---- -------employees-------
  date/time   | read write prv_r prv_w
18-05 10:32:35| 327     0   160   160
18-05 10:32:55| 327     0   160   160
18-05 10:32:58| 327     0   160   160
```
