global boto, datetime, pytz, aws_region, dynamodb_table

import boto.ec2.cloudwatch
import datetime
import pytz

aws_region = os.getenv('AWS_REGION')
dynamodb_table = os.getenv('DSTAT_DYNAMODB_TABLE')

class dstat_plugin(dstat):
    def __init__(self):
        self.name   = dynamodb_table
        self.type   = 'd'
        self.width  = 5
        self.scale  = 1000
        self.vars   = ('read', 'write', 'prv_r', 'prv_w')
        self.__conn = None
        self.__prev_update = None

    def check(self):
        if not aws_region:
            raise Exception, 'Plugin needs AWS Region'
        if not dynamodb_table:
            raise Exception, 'Plugin needs TableName'

    def extract(self):
        if not self.__update_require():
          return

        self.__connect()
        end_time   = datetime.datetime.now(tz=pytz.utc)
        start_time = end_time - datetime.timedelta(minutes=14)
        self.val['read']  = self.__get_consumed_read_capacity_units(start_time, end_time)
        self.val['write'] = self.__get_consumed_write_capacity_units(start_time, end_time)
        self.val['prv_r'] = self.__get_provisioned_read_capacity_units(start_time, end_time)
        self.val['prv_w'] = self.__get_provisioned_write_capacity_units(start_time, end_time)

    def __update_require(self):
        now = datetime.datetime.now(tz=pytz.utc)

        if not self.__prev_update:
            self.__prev_update = now
            return True
        elif self.__prev_update < (now - datetime.timedelta(seconds=15)):
            self.__prev_update = now
            return True
        else:
          return False

    def __connect(self):
        if not self.__conn:
            self.__conn = boto.ec2.cloudwatch.connect_to_region(aws_region)

    def __get_consumed_read_capacity_units(self, start_time, end_time):
        metrics = self.__get_metric_statistics(
                         start_time,
                         end_time,
                         'ConsumedReadCapacityUnits',
                         'Sum')

        if metrics > 0.0:
            metrics = metrics / 300.0

        return metrics

    def __get_consumed_write_capacity_units(self, start_time, end_time):
        metrics = self.__get_metric_statistics(
                         start_time,
                         end_time,
                         'ConsumedWritCapacityUnits',
                         'Sum')
        if metrics > 0.0:
            metrics = metrics / 300.0

        return metrics

    def __get_provisioned_read_capacity_units(self, start_time, end_time):
        metrics = self.__get_metric_statistics(
                         start_time,
                         end_time,
                         'ProvisionedReadCapacityUnits',
                         'Average')

        return metrics

    def __get_provisioned_write_capacity_units(self, start_time, end_time):
        metrics = self.__get_metric_statistics(
                         start_time,
                         end_time,
                         'ProvisionedWriteCapacityUnits',
                         'Average')

        return metrics

    def __get_metric_statistics(self, start_time, end_time, metric_name, statistics):
        metrics = self.__conn.get_metric_statistics(
                                period=300,
                                start_time=start_time,
                                end_time=end_time,
                                metric_name=metric_name,
                                namespace='AWS/DynamoDB',
                                statistics=[statistics],
                                dimensions={'TableName': dynamodb_table},
                                unit='Count')

        metrics = sorted(metrics, cmp=lambda x, y:cmp(x['Timestamp'], y['Timestamp']), reverse=True)

        if not metrics:
            return 0.0
        elif len(metrics) < 1:
            return 0.0
        else:
            return metrics[0][statistics]
